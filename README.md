# React Native - Coding Challenge

Cuddlynest is looking for people who can build awesome products, so we created this challenge to test our candidates' overall developer skills.

## Instructions

#### 1. Build your app

You have 1 week to complete the challenge.

_Implementation and code structure will be evaluated._

#### 2. Submit your challenge

Follow these instructions to submit your challenge.

- Setup your Development Environment ([React Native - Getting Started guide](https://facebook.github.io/react-native/docs/getting-started.html))
- Write your Code
- Write tests
- Commit your Changes
- Fork the Challenge Repository
- Issue a Pull Request

#### 3. Impress us with your skills

## Challenge

Cuddlynest needs a mobile app to manage data of their employees.

## Requirements

Your app should be able to complete the following tasks:

- Add/Remove/Edit an Employee (firstName, lastName, position, department, phoneNumber, isActive)
- List all of the Employees with search/filter by giving firstName or lastName
- Persist data using Redux

## Grading

The grading of the app will be based off of three criteria:

- **70%** - Overall code quality
- **30%** - Data Management and Store
